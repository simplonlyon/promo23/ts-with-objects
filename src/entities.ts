/**
 * En typescript, une interface ou type (les deux sont possibles et s'utilisent
 * quasiment de la même manière) est une manière de créer des types personnalisés
 * qu'on pourra utiliser ensuite pour typer nos variables, tableaux, paramètres et
 * retour de fonction
 * On déclare dedans la liste des propriétés et le type de chacune d'entre elle (on
 * peut indiqué des propriétés optionnelles avec un ?)
 * Par convention, le nom des interfaces/type est en PascalCase/CapitalizedCamelCase
 * (plus pratique pour les différencier des noms de variables)
 */
export interface Person {
    name: string;
    age: number;
    firstName: string;
    address?: Address;
}
export interface Address {
    street: string;
    city: string;
    zipCode: string;
}


export interface Dog {
    id?:number;
    name:string;
    breed:string;
    birthdate: Date;
}

export interface CustomizeUI {
    mainColor:string;
    textSize:string;
}

export interface Ingredient {
    label:string;
    quantity:number;
    unit:string;
}

export interface Recipe {
    title:string;
    serves:number;
    ingredients:Ingredient[];
    steps:string[];
}

export interface Message {
    username:string;
    date:Date;
    content:string;
}