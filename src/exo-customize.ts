import { CustomizeUI } from "./entities";


const inputColor = document.querySelector<HTMLInputElement>('#mainColor');
const inputTextSize = document.querySelector<HTMLInputElement>('#textSize');

updateUI({mainColor: 'blue', textSize: '1em'});

const storedConfig = localStorage.getItem('config');
if(storedConfig) {
    const parsedConfig:CustomizeUI = JSON.parse(storedConfig);
    updateUI(parsedConfig);

    if(inputColor && inputTextSize) {
        inputColor.value = parsedConfig.mainColor;
        inputTextSize.value = parsedConfig.textSize;
        onRangeChange()
    }

}

const form = document.querySelector<HTMLFormElement>('.modal form');
form?.addEventListener('submit', (event)=> {
    event.preventDefault();
    if(inputColor && inputTextSize) {
        const config:CustomizeUI = {
            mainColor: inputColor.value,
            textSize: inputTextSize.value
        }
        localStorage.setItem('config', JSON.stringify(config));
        updateUI(config);
    }
});

inputTextSize?.addEventListener('input', onRangeChange)

function onRangeChange(){
    const spanValue = document.querySelector<HTMLElement>('#rangeValue');
    if(spanValue && inputTextSize) {
        spanValue.textContent = inputTextSize.value;
    }
}

function updateUI(config:CustomizeUI) {
    const navBar = document.querySelector<HTMLElement>('#main-navbar');
    const paras = document.querySelectorAll<HTMLParagraphElement>('p');

    if(navBar) {
        navBar.style.backgroundColor = config.mainColor;
    }

    for (const item of paras) {
        item.style.fontSize = config.textSize+'em';
    }
}