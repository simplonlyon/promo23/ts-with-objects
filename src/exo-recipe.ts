import { Ingredient, Recipe } from "./entities";
/**
 * La recette par défaut qu'on voudra afficher pour cette page
 */
const defaultRecipe:Recipe = {
    title:'Bread',
    serves: 5,
    ingredients:[
        {label: 'sourdough', quantity:100, unit: 'gr'},
        {label: 'flour', quantity:500, unit: 'gr'},
        {label: 'water', quantity:320, unit: 'gr'},
        {label: 'salt', quantity:10, unit: 'gr'}
    ],
    steps:[
        "Previous day, feed your sourdough",
        "mix everything",
        "knead the dough"
    ]
}


//On commence par appeler le drawRecipe qui va se baser sur l'objet donné en argument pour générer l'affichage
drawRecipe(defaultRecipe);

const btnPlus = document.querySelector<HTMLButtonElement>('#plus');
const btnMinus = document.querySelector<HTMLButtonElement>('#minus');

btnPlus?.addEventListener('click', () => {
    changeServing(1);
    drawRecipe(defaultRecipe);
});
btnMinus?.addEventListener('click', () => {
    if(defaultRecipe.serves > 1) {

        changeServing(-1);
        drawRecipe(defaultRecipe);
    }
});

/**
 * Fonction qui convertit un objet ingrédient en chaîne de caractères HTML
 * @param ingredient l'ingrédient dont on souhaite générer le html
 * @returns une chaîne de caractère html qu'il faudra ajouter à un parent via un innerHTML
 */
function drawIngredient(ingredient:Ingredient):string{

    return `<li>${ingredient.label} : ${ingredient.quantity}${ingredient.unit}</li>`;
}
/**
 * Fonction qui va prendre une recette en argument et utiliser ses propriétés pour générer
 * son affichage, mettre les différentes propriétés dans les bonnes balises, etc.
 * @param recipe la recette dont on souhaite généré l'affichage
 */
function drawRecipe(recipe:Recipe):void {
    const h1 = document.querySelector<HTMLElement>('h1');
    const servesSpan = document.querySelector<HTMLElement>('#serves');
    const ingredientUl = document.querySelector<HTMLElement>('#ingredients');
    const stepOl = document.querySelector<HTMLElement>('#steps');

    if(h1) {
        h1.innerHTML = recipe.title;
    }
    if(servesSpan) {
        servesSpan.innerHTML = String(recipe.serves);
    }
    if(ingredientUl) {
        ingredientUl.innerHTML = '';
        for (const itemIngr of recipe.ingredients) {
            const html = drawIngredient(itemIngr);
            ingredientUl.innerHTML += html;
        }
    }
    if(stepOl) {
        stepOl.innerHTML = '';
        for (const itemStep of recipe.steps) {
            stepOl.innerHTML += `<li>${itemStep}</li>`;
        }
    }

}


function changeServing(diff:number) {
    for(const ingredient of defaultRecipe.ingredients) {
        const serveOne = ingredient.quantity / defaultRecipe.serves;
        ingredient.quantity = serveOne * (defaultRecipe.serves+diff);
    }

    defaultRecipe.serves += diff;
}


//version avec create element, qui est un peu plus longue à écrire, mais sans doute plus
//versatile, mais qui est aussi un peu plus éloigné de ce à quoi ressemblera le code react par la suite
//(donc en résumé, techniquement c'est mieux de faire ça je pense, mais histoire de faire autrement et de s'habituer à React, on fait l'autre)
// function drawIngredient(ingredient:Ingredient):HTMLElement{
//     const li = document.createElement('li');

//     li.innerHTML = `${ingredient.label} : ${ingredient.quantity}${ingredient.unit}`;
//     return li;
// }
//qui s'utiliserait comme ça
// document.body.appendChild(drawIngredient({label:'farine', quantity:500, unit:'gr'}));
