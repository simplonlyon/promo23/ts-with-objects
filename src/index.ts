import { Address, Person } from "./entities";


let address:Address = {
    street: 'Rue Antoine Primat',
    city: 'Villeurbanne',
    zipCode: '69100'
}
/**
 * Ici, j'utilise mon interface Person pour indiquer que cette variable ne pourra
 * contenir que des objets qui ressemblent à des Person
 */
let person:Person = {
    firstName: 'Paulie',
    name: 'Bloupy',
    age: 46,
    address: address
}



console.log(person.firstName);
console.log(person.address?.city);
console.log(person);

//Ici, j'indique que mon tableau ne pourra contenir que des objets de type Person
const tableau:Person[] = [];


tableau.push({name: 'test', firstName:'toust', age: 10});
