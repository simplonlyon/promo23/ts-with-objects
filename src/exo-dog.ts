import { Dog } from "./entities";

let dogList: Dog[] = [
    { name: 'Fido', breed: 'Corgi', birthdate: new Date(), id: 1 },
    { name: 'Teckie', breed: 'Teckel', birthdate: new Date(), id: 2 }
];
/**
 * Variable qui contient le prochain id à assigné à un nouveau chien
 */
let currentId = 1;

let selectedDog: Dog | undefined;


const btnDelete = document.querySelector<HTMLButtonElement>('#btn-delete');
btnDelete?.addEventListener('click', () => {
    if(selectedDog) {
        removeDog(selectedDog);
        selectedDog = undefined;
        display();
    }
});

display();

const nameInput = document.querySelector<HTMLInputElement>('#name');
const breedInput = document.querySelector<HTMLInputElement>('#breed');
const birthdateInput = document.querySelector<HTMLInputElement>('#birthdate');
const form = document.querySelector<HTMLFormElement>('form');

form?.addEventListener('submit', (event) => {
    event.preventDefault();

    if (nameInput?.value && breedInput?.value && birthdateInput?.value) {

        const dog: Dog = {
            name: nameInput.value,
            breed: breedInput.value,
            birthdate: new Date(birthdateInput.value)
        }
        addDog(dog);
        display();
    }

    console.log(dogList);

    /*
     Alternativement on peut récupérer les données d'un formulaire avec un FormData qui attend en
     argument le formulaire en question et qui renverra un objet permettant d'accéder
     aux différentes valeurs du formulaire en se basant sur les name des inputs.
     Ca permet de ne pas avoir à faire de querySelector pour chaque input
     */
    // const formData = new FormData(form);
    // console.log(
    // formData.get('name'),
    // formData.get('breed'),
    // formData.get('birthdate')
    //)
});


/**
 * Fonction qui va attendre un chien en argument, lui assigner une id unique et l'ajouter
 * la liste dogList
 * @param dog le chien à ajouter à la liste
 */
function addDog(dog: Dog): void {
    // currentId++;
    dog.id = currentId++;
    dogList.push(dog);
}

function removeDog(dog:Dog): void {
    dogList = dogList.filter(item => item != dog);
}

/**
 * Fonction permettant de générer les éléments HTML requis pour faire l'affichage
 * d'un chien (ici on choisit de l'afficher sous forme de card boostrap)
 * @param dog le chien dont on veut générer le HTML
 * @returns L'élément HTML représentant un chien donné
 */
function renderDog(dog: Dog): HTMLElement {
    const divCol = document.createElement('div');
    divCol.classList.add('col-sm-6', 'col-lg-3');

    const divCard = document.createElement('div');
    divCard.classList.add('card');
    divCol.append(divCard);

    const divCardHeader = document.createElement('div');
    divCardHeader.classList.add('card-header');
    divCard.append(divCardHeader);

    const divH3 = document.createElement('h3');
    divH3.innerHTML = dog.name;
    divCardHeader.append(divH3);

    const divCardBody = document.createElement('div');
    divCardBody.classList.add('card-body');
    divCard.append(divCardBody);

    const pBreed = document.createElement('p');
    pBreed.innerHTML = dog.breed;
    divCardBody.append(pBreed);

    const pBirthdate = document.createElement('p');
    pBirthdate.innerHTML = dog.birthdate.toLocaleDateString();
    divCardBody.append(pBirthdate);

    if (selectedDog == dog) {
        divCard.classList.add('bg-secondary');
    }


    divCard.addEventListener('click', () => {
        if(selectedDog == dog) {
            selectedDog = undefined;
        } else {
            selectedDog = dog;
        }
        display();

    });

    return divCol;
}

function display() {

    const section = document.querySelector('section');
    if (section) {
        section.innerHTML = '';
        section.appendChild(renderList(dogList));
    }
    if(btnDelete) {

        if(selectedDog) {
            btnDelete.style.display = 'block'
        } else {
            btnDelete.style.display = 'none'
            
        }
    }
}

/**
 * Fonction qui va itérer sur un array de chien et générer le html pour chacun
 * des chiens donnés
 * @param dogs la liste de chien dont on veut générer le html
 * @returns Une div contenant tous les éléments HTML représentant les chiens
 */
function renderList(dogs: Dog[]): HTMLElement {
    const row = document.createElement('div');
    row.classList.add('row', 'g-3');
    for (const dog of dogs) {
        const dogDiv = renderDog(dog);
        row.append(dogDiv);
    }
    return row;
}
