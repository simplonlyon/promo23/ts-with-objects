import { Message } from "./entities";

let username = '';
const messages: Message[] = [
    { username: 'Fake Name', content: 'Hello', date: new Date() },
    { username: 'Fake Name', content: 'How are you', date: new Date() },
    { username: 'Fake Name', content: 'I\'m lonely', date: new Date() }
];
const textAreaContent = document.querySelector<HTMLTextAreaElement>('textarea');
const formMessage = document.querySelector<HTMLFormElement>('#add-message');
const formLogin = document.querySelector<HTMLFormElement>('#login form');

const sectChatRoom = document.querySelector<HTMLElement>('#chat-room');
const sectLogin = document.querySelector<HTMLElement>('#login');

const stored = localStorage.getItem('username');
if(stored) {
    username = stored;
    toggleDisplay()
}

updateMessages();

formLogin?.addEventListener('submit', (event) => {
    event.preventDefault();
    const inputUsername = document.querySelector<HTMLInputElement>('#username');
    if(inputUsername) {
        username = inputUsername.value;
        localStorage.setItem('username', username);
    }
    toggleDisplay();
});

formMessage?.addEventListener('submit', (event) => {
    event.preventDefault();
    if(textAreaContent) {
        addMessage(textAreaContent.value);
        updateMessages();

    }
});


function toggleDisplay() {
  document.body.classList.toggle('logged');
}

function addMessage(content:string) {
    let message: Message = {
        content,
        username,
        date: new Date()
    };
    messages.push(message);
}

function updateMessages() {
    const ulMessage = document.querySelector<HTMLUListElement>('#messages');
    if (ulMessage) {
        ulMessage.innerHTML = '';
        for (const message of messages) {

            ulMessage.append(displayMessage(message));
        }

    }
}
function displayMessage(message: Message) {
    const li = document.createElement('li');
    li.innerHTML = `<span class="date">${message.date.toLocaleString()}</span> <span class="username">${message.username}</span> : ${message.content}`;
    return li;
}

