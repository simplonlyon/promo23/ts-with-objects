# ts-with-objects

Projet typescript utilisant des objets inline et des interfaces pour les typer

## Exercices Chiens ([html](public/exo-dog.html)/[ts](../../src/exo-dog.ts))
### I. Le type, le tableau et l'ajout
1. Créer un fichier exo-dog.html et exo-dog.ts
2. Dans le fichier entities ou autre, créer et exporter une interface Dog qui aura comme propriétés : un id en number optionnel, un name en string, un breed en string et birthdate en Date
3. Dans le fichier exo-dog.ts, créer une variable qui contiendra un tableau de Dog
4. Créer une fonction addDog qui va attendre un Dog en argument et dont le travail sera de rajouter le chien en question dans le tableau, mais surtout de lui assigner un id unique (plusieurs manière, l'une d'elle serait d'avoir une variable currentId à l'extérieur de la fonction qu'on incrémente à chaque addDog) [pour vous aider, vous pouvez commencer par faire que la fonction mette 1 comme id à tous les chiens donnés en paramètre]
### II. Bootstrap, le formulaire et son event
1. Dans le HTML, charger le style bootstrap et créer un formulaire avec 3 input (pour name/breed/birthdate) et un button pour submit
2. Dans le ts, sélectionner le formulaire et lui rajouter un eventListener au submit et commencer déjà par faire en sorte d'afficher les valeurs en console
3. Prendre les valeurs en question et les donner à manger à la fonction addDog sous forme d'objet (attention, il faudra probablement convertir la birthdate de string vers Date, vous pouvez chercher sur internet comment on fait ça)
4. Faire en sorte d'afficher le tableau en console après chaque validation du formulaire 
### III. Affichage d'un chien
1. Créer une fonction renderDog qui va attendre en argument un objet de type Dog et qui va renvoyer un objet de type HTMLElement
2. Dans cette fonction, faire des createElement, des textContent et tout pour créer un élément pour le chien en question (on assignera aux différents éléments créés les valeurs de l'objet chien)
3. Pour tester cette fonction, on l'appel en lui donnant un chien en dur (par exemple : {id: 1, name: 'Fido', breed:'Corgi', birthdate: new Date()} et on append le résultat de cette fonction sur le body ou autre
### IV. La Liste de chiens
1. Créer une nouvelle fonction renderList qui va attendre en argument un tableau de chien et renvoyer un HTMLElement
2. dans cette fonction, créer une div avec une classe row puis faire une boucle sur le tableau donné en argument et pour chacun des chiens, faire un appel à la fonction renderDog et en faire un append sur la div 
3. Tester la fonction en l'appelant et en faisant un append de l'élément renvoyé sur le body du document ou autre
4. Faire en sorte d'afficher la liste de chien au lancement de la page et de la mettre à jour à chaque modification de la liste (genre quand on submit le form)
### V. Sélectionner un chien
1. Dans le typescript, rajouter une variable selectedDog qui aura comme type Dog|undefined (pasque il n'y aura pas toujours un chien de sélectionné, donc la variable doit pouvoir être vide, et est initialisée vide d'ailleurs)
2. Dans le renderDog, rajouter un event au click sur la col/la card/un bouton dans la card/peu importe, et dans cet event, faire que ça assigne le chien cliqué à la variable selectedDog qu'on vient de créer
3. Dans la fonction renderDog toujours, rajouter une condition qui va vérifier si le chien actuellement sélectionné est égal au chien donné en paramètre à la fonction, et si oui, rajouter une classe sur ce que vous voulez, ou du style directement peu importe, bref un truc sur la card qui va permettre de la distinguer visuellement
4. Pas le plus optimisé, mais c'est histoire que le concept rentre : dans l'event au click, faire en sorte, une fois le chien sélectionné, de relancer le renderList pour mettre à jour l'affichage
5. Modifier encore l'event au click pour faire que si le chien qu'on vient de cliquer correspond déjà au chien sélectionné, alors on "désélectionne" en remettant la variable vide
### VI. Supprimer le chien sélectionné 
1. Créer une fonction removeDog qui va attendre un Dog en argument et dedans on va faire un filter pour retirer un chien de dogList. Vu que la syntaxe est un peu particulière je vous la donne direct (on aura l'occasion d'en refaire des dizaines de fois par la suite) : dogList = dogList.filter(item => item != dog) (en gros, ça assigne à la liste une nouvelle liste filtrer, en considérant qu'on ne garde dans cette liste filtré que les chiens qui ne correspondent pas à celui qu'on souhaite supprimer)
2. Rajouter un bouton Delete quelque part dans le HTML, puis le sélectionner dans le typescript et lui rajouter un eventListener au click
3. Dans cet event listener, faire en sorte d'appeler la fonction removeDog en lui donnant le chien actuellement sélectionné puis ensuite relancer la mise à jour de l'affichage
4. Modifier la mise à jour de l'affichage pour faire que le bouton delete ne soit affiché que si on a un chien sélectionné 

### VII. Supprimer le chien sélectionné 
1. Créer une fonction removeDog qui va attendre un Dog en argument et dedans on va faire un filter pour retirer un chien de dogList. Vu que la syntaxe est un peu particulière je vous la donne direct (on aura l'occasion d'en refaire des dizaines de fois par la suite) : dogList = dogList.filter(item => item != dog) (en gros, ça assigne à la liste une nouvelle liste filtrée en considérant qu'on ne garde dans cette liste filtré que les chiens qui ne correspondent pas à celui qu'on souhaite supprimer)
2. Rajouter un bouton Delete quelque part dans le HTML, puis le sélectionner dans le typescript et lui rajouter un eventListener au click
3. Dans cet event listener, faire en sorte d'appeler la fonction removeDog en lui donnant le chien actuellement sélectionné puis ensuite relancer la mise à jour de l'affichage
4. Modifier la mise à jour de l'affichage pour faire que le bouton delete ne soit affiché que si on a un chien sélectionné 


## Exercice UI Customizer ([html](public/exo-customize.html)/[ts](../../src/exo-customize.ts))
1. Dans le projet ts-with-objects, créer un nouveau fichier html exo-customize.html et le lier à un nouveau fichier ts exo-customize.ts
2. Rajouter au HTML une navbar bootstrap un peu basique, faire juste en sorte qu'elle ait un bouton/icon de customisation (genre la ptite roue dentée) quelque part. Mettre aussi un ou deux paragraphes
3. Rajouter le javascript de bootstrap et rajouter une modal dans le HTML qui s'ouvrira quand on click sur le bouton customize. Dans cette modal, faire un formulaire qui aura un input type color Main Color et un input de type range (yen a un dans bootstrap) Text Size
4. Créer une interface CustomizeUI dans le fichier entities.ts qui aura pour le moment comme propriété un mainColor en string et un textSize en string également
5. Dans le fichier TS, créer une fonction updateUI qui va attendre un paramètre config de type CustomizeUI, et dans cette fonction faire les querySelector qui vont bien pour changer la background-color de la NavBar par la mainColor de config et de changer la taille des textes des paragraphes par textSize (tester cette fonction en l'appelant avec un objet d'exemple)
6. Rajouter un eventListener sur le formulaire de la modal qui ira chercher les values des input et s'en servira pour appeler la fonction updateUI
7. Au moment du submit, faire également en sorte de stocker la configuration donnée dans le localStorage (il faudra faire unJSON.stringify sur l'objet pour le stocker correctement)
8. Faire qu'au lancement de la page, on récupère la config sauvegardée dans le localStorage, la JSON.parse si elle existe et s'en servir pour faire un updateUI

**Bonus :** Faire en sorte que lorsqu'on change les valeurs du formulaire, ça applique directement les changements en mode "prévisualisation" mais que ceux ci soient maintenus seulement si on valide le formulaire et annulés si on ferme juste la modal

## Exercice Recettes/Ingrédient ([html](public/exo-recipe.html)/[ts](../../src/exo-recipe.ts))
### Affichage de la recette
1. Dans le entities.ts créer les interface Recipe (title,ingredients,serves et steps) et Ingredient (label, quantity, unit)
2. Créer des fichier exo-recipe html et ts, dans le ts, créer un objet recipe de type Recipe et mettre quelques valeurs par défaut dedans (vous pouvez reprendre ma recette de pain, c'est cadeau)
3. Dans le html, mettre les différentes balises "statiques" qui existeront pour la recettes (les trucs qu'on ciblera avec le TS pour mettre des valeurs dedans)
4. Créer une fonction drawIngredient qui va attendre un Ingredient en argument et qui va renvoyer une chaîne de caractère avec les balises HTML qui vont bien (genre des li) avec les valeurs de l'ingrédient dedans (on peut tester cette fonction en l'appelant et en faisant un petit console.log pour voir si on a bien le html qu'on s'attend à avoir)
5. Créer une fonction drawRecipe, qui va attendre une Recipe en argument et qui va cibler les différents élément html pour y mettre les valeurs de la recette (genre son titre, le nombre de personnes). Appeler cette fonction au lancement de la page en lui donnant notre recette par défaut
6. Maintenant faire en sorte dans le drawRecipe de boucler sur les ingrédients et pour chaque ingrédient appeler le drawIngredient et concaténer le résultat au innerHTML de l'élément où on veur les afficher
7. On peut faire pareil pour les steps, mais là on a pas de fonction drawStep.... donc à voir si on en fait une ou pas 

### Changement des quantités
1. Créer une fonction changeServing qui va attendre un number en argument par exemple, et dans cette fonction on va boucler sur tous les ingrédients de notre recette, et pour chaque ingrédient on va diviser la quantity par le serves de la recette initial et stocker cette valeur, puis modifier la quantity pour mettre la valeur stockée juste avant multiplier par le serving actuel + l'argument number de la fonction. A la fin de la fonction, on modifie le serves de la recipe en lui additionnant l'argument
2. Cibler les bouton + et - et faire que quand on click dessus, ça déclenche le changeServing en donnant 1 dans un cas et -1 dans l'autre puis relancer la fonction drawRecipe pour mettre à jour l'affichage
3. On peut modifier notre drawIngredient pour y faire un arrondi de la quantité 


## Lonely Chat ([html](public/exo-chat.html)/[ts](../../src/exo-chat.ts))
 * Faire une interface pour les messages
* Faire en sorte d'avoir quelques messages par défaut dans le script et de les afficher via une fonction qui boucle dessus (avec des createElement j'pense que ça sera plus pertinent pour cet exo)
* Faire donc un formulaire permettant de rajouter un nouveau message (et que quand on le valide, ça lui assigne comme date la Date de maintenant, donc le formulaire n'aura que un champ pour le content au final)
* Faire que quand on arrive sur la page, ça nous demande notre username avant de nous permettre de voir les messages/poster des messages, on aura donc 2 formulaires dans notre HTML au final, un pour le username et l'autre pour poster un message (2 sections, une qu'est display none et l'autre display block et on inverse quand on valide le formulaire de username, ça peut l'faire)
* Pourquoi pas faire que le username soit stocké en localStorage pour faire que si on a déjà un username, il nous le redemande pas et nous mette directement l'ui des messages
